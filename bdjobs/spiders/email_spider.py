import scrapy
import re


class EmailSpider(scrapy.Spider):
    name = 'emails'

    def start_requests(self):
        yield scrapy.Request(url='http://www.bdjobs.com', callback=self.parse_categories)

    def parse_categories(self, response):
        categories = response.css(".category-list ul li a::attr(href)").extract()
        for category in categories:
            yield scrapy.Request(url=category, callback=self.parse_jobs)

    def parse_jobs(self, response):
        btns = response.css(".norm-jobs-wrapper::attr(onclick), .sout-jobs-wrapper::attr(onclick)").extract()
        for btn in btns:
            url = 'http://jobs.bdjobs.com/jobdetails.asp?' + btn[btn.index("('")+2:-3]
            yield scrapy.Request(url=url, callback=self.parse)
        next_page = response.css(".pagination li a.prevnext::attr(href)").extract()
        if len(next_page) is not 0:
            np = re.search(r'GoPage\((\d+)\)', next_page[1])
            if np:
                nn = np.group(1)
                cat_id = re.search('fcatId=(\d+)', response.url)
                if cat_id:
                    fcat = cat_id.group(1)
                    frm_data = {
                        'txtsearch': '',
                           'fcat': fcat,
                           'qOT': '0',
                           'iCat': '0',
                           'Country': '0',
                           'qPosted': '0',
                           'qDeadline': '0',
                           'Newspaper': '0',
                           'qJobSpecialSkill': '-1',
                           'qJobNature': '0',
                           'qJobLevel': '0',
                           'qExp': '0',
                           'qAge': '0',
                           'hidOrder': '',
                           'pg': str(nn),
                           'hidJobSearch': 'JobSearch',
                           'MPostings': '',
                           'ver': ''
                    }
                    yield scrapy.FormRequest(url=response.url, formdata=frm_data, callback=self.parse_jobs)

    def parse(self, response):
        company = response.css(".company-name::text").extract_first()
        email = response.css(".guide .or strong::text").extract_first()
        yield {
            'company': company,
            'email': email
        }
