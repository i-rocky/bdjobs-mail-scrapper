# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import MySQLdb


class BdjobsPipeline(object):
    def __init__(self):
        self.conn = MySQLdb.connect("localhost", "root", "", "emails", charset="utf8", use_unicode=True)
        self.c = self.conn.cursor()

    def process_item(self, item, spider):
        print("Result:", item['company'], item['email'])
        if item['email'] is not None:
            self.c.execute("""INSERT INTO mails (company, email) VALUES (%s, %s)""", (item['company'], item['email']))
        return item
